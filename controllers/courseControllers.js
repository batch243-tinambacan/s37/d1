const Course = require("../models/Course");
const auth =require("../auth")


/*
	Steps:
	1. Create a new Course object using mongoDb
	2. 
*/


module.exports.addCourse = (request, response) =>{
	
	let token = request.headers.authorization;
	const userData = auth.decode(token); //decrypt
	console.log(userData);


	let newCourse = new Course({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		slots: request.body.slots
	})

	if(userData.isAdmin){
		newCourse.save().then(result =>{
			console.log(result);
			response.send(true);
		}).catch(error => {
			console.log(error);
			response.send(false);
		})
	}
	else{
		response.send("You do not have an admin role!")
	}

		
}
	

// Retrieve a;; active courses

module.exports.getAllActive = (request, response) =>{
	return Course.find({isActive: true}) //access course model/schema
	.then(result => {
		response.send(result);
	}).catch(err => {
		response.send(err)
	})
}


// retrieving a specific course

module.exports.getCourse = (request, response) => {
	const courseID = request.params.courseId

	return Course.findById(courseID).then(result => {
		response.send(result);

	}).catch(err =>{
		response.send(err)
	})
}

// update a course 

module.exports.updateCourse = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);
	console.log(userData);

	let updatedCourse = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		slots: request.body.slots
	}
	
	const courseID = request.params.courseId;

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(courseID, updatedCourse, {new: true}).then(result=>{
			response.send(result)
		}).catch(err =>{
			response.send(err);
		})
	}
	else{
		return response.send("You dont have access to this page")
	}
}



// archive a course 

module.exports.archiveCourse = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);
	console.log(userData);

	const courseID = request.params.courseId;
	let updateIsActive = {
		isActive: request.body.isActive
	}

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(courseID, updateIsActive, {new: true}).then(result=>{
			response.send(true)
		}).catch(err =>{
			response.send(false);
		})
	}
	else{
		return response.send("You dont have access to this page")
	}
}

// get all courses regardless of status

module.exports.getAllCourses = (request, response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	if(!userData.isAdmin){
		return response.send("Sorry, you don't have access to this page!")
	}
	else{
		return Course.find({}).then(result => response.send(result))
		.catch(err => {
			console.log(err);
			response.send(err)
		})
	}
}




