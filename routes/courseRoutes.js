const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth.js")

// adding course
router.post("/", auth.verify, courseControllers.addCourse);

// All active courses
router.get("/allActiveCourses", courseControllers.getAllActive);

//Retrieve all Courses
router.get("/allCourses", auth.verify, courseControllers.getAllCourses);


//**** with params below ****//


// Specific course
router.get("/:courseId", courseControllers.getCourse);

// update a specific course
router.put("/update/:courseId", auth.verify, courseControllers.updateCourse);

// archiving a course
router.patch("/:courseId/archive", auth.verify, courseControllers.archiveCourse);



module.exports = router;